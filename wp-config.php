<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'estipo5_quadric' );

/** Database username */
define( 'DB_USER', 'estipo5_quadricuser' );

/** Database password */
define( 'DB_PASSWORD', 'c@(n8D2[-%*!' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'lO#_RoTfEXV?n0v[<Kwmsmq0bMm^KxTBI[wP01G:+@i.:HT1vh9!R7)5:fJIj55&' );
define( 'SECURE_AUTH_KEY',  '$aG_Cfmct]}w):AAR`H9PZbD.7nOm0]+QDgHZujj*^3G$$J&bbWlLnl@~>vLRWi2' );
define( 'LOGGED_IN_KEY',    ':,W%5C,cLb-ld-Znhq4!}`seJHi2&k=PEe31KrxIy1=]uW/?Z07Jks[$p3awS s}' );
define( 'NONCE_KEY',        '{Bo;YLz_bP@t=x5K*=$fK^YZ}b0me)Gtiiyd=atw puouQmVpXS$<N&CuC+[@S_N' );
define( 'AUTH_SALT',        'WaAX<cR-+lRs!$6~0u{7iEk_>RAk6ybB*XMWY/nCe1%p=v3XF4YZv_HAHuzBF5p,' );
define( 'SECURE_AUTH_SALT', 'Mys>v^Jo+X`CuT)SxM#_To# .o,T;3GgxN}#u<2*X]EXX_g(nvRRZ;+j%0!k,}3{' );
define( 'LOGGED_IN_SALT',   'b /|y-T+mvHOJ*a`uXiIOpVQKkb(Tpgw3#<xE:c/c<(T2@`[,+VHt1&FfXCq0ohT' );
define( 'NONCE_SALT',       'N?o9q~%Tpc45krz:Z_~eHyzHM8k.F.*E,gk9#~>oBu6#[;^cSFE&.e[eD&iEOK-h' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
